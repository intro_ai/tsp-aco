# Project 3: TSP using ACO
# Team: Alexander Welch and Maryna Sevryukova
# Course: CAP4630
# Date: 7/9/2023

import numpy as np
import matplotlib.pyplot as plt
import math
import csv
from aco_algorithm.graph import Graph
from aco_algorithm.city import generate_cities, create_distances
from aco_algorithm.aco import ACO


def main():
    while True:
        # User selects the type of ACO variant they want to use
        aco_variant = get_user_input('Choose ACO variant: 1. Basic ACO, 2. Elitist Ant System: ', type_=int, min_=1, max_=2)

        # Generating the problem (TSP) instance
        num_cities = get_user_input('Enter number of cities: ', type_=int, min_=1)
        cities = generate_cities(num_cities)
        distances = create_distances(cities)

        # Initialize the pheromone matrix
        initial_pheromone = np.ones((num_cities, num_cities))

        # Construct the graph
        graph = Graph(num_cities, distances, initial_pheromone)

        # Get the parameters of the ACO algorithm from the user
        ant_count = get_user_input('Enter ant count: ', type_=int, min_=1)
        generations = get_user_input('Enter number of generations: ', type_=int, min_=1)
        alpha = get_user_input('Enter pheromone importance factor: ', type_=float, min_=0)
        beta = get_user_input('Enter distance importance factor: ', type_=float, min_=0)
        rho = get_user_input('Enter pheromone evaporation rate: ', type_=float, min_=0)
        q = get_user_input('Enter total amount of pheromone that each ant lays on the trail per tour: ', type_=int, min_=1)
        max_stagnation = get_user_input('Enter stopping condition: ', type_=int, min_=1)

        # Check for which variant of ACO to run
        if aco_variant == 1:
            # Create the ACO instance
            aco = ACO(ant_count=ant_count, generations=generations, alpha=alpha, beta=beta, rho=rho, q=q, e=0,
                      max_stagnation=max_stagnation)

            # Run the basic ACO algorithm and get the results
            best_solution, best_cost, all_paths, best_costs = run_aco(aco, graph)

        else:
            # For Elitist Ant System, we also need the additional pheromone for the best solution
            e = get_user_input('Enter amount of extra pheromone for the best solution: ', type_=int, min_=0)

            aco = ACO(ant_count=ant_count, generations=generations, alpha=alpha, beta=beta, rho=rho, q=q, e=e,
                      max_stagnation=max_stagnation)

            # Run the Elitist Ant System and get the results
            best_solution, best_cost, all_paths, best_costs = run_elitist_aco(aco, graph)

        print(f"\nBest route: {best_solution}")
        print(f"Cost of best route: {best_cost}")
        print("\n\n")

        # Plot the paths and the evolution of the costs
        plot_path(aco, best_solution, cities, all_paths)
        plot_costs(best_costs, 'best_costs.png')

        # Ask the user if they want to run the program again
        should_continue = get_user_input('Do you want to try again? (y/n): ', range_=('y', 'n'))
        if should_continue.lower() == 'n':
            break


def get_user_input(prompt, type_=None, min_=None, max_=None, range_=None):
    """
    This function asks the user for input and validates it based on the provided criteria.

    Parameters:
    prompt (str): The prompt message for the user.
    type_ (type, optional): The required type of the input. Defaults to None.
    min_ (int or float, optional): The minimum acceptable value. Defaults to None.
    max_ (int or float, optional): The maximum acceptable value. Defaults to None.
    range_ (range, optional): An acceptable range of values. Defaults to None.

    Returns:
    int or float or str: The user's input, converted to the required type if specified.
    """
    # Check if both min_ and max_ are provided, and max_ is less than min_
    if min_ is not None and max_ is not None and max_ < min_:
        raise ValueError("min_ must be less than or equal to max_.")

    while True:
        # Prompt the user for input
        user_input = input(prompt)

        # If a specific type is expected, try to convert the input to that type
        if type_ is not None:
            try:
                user_input = type_(user_input)

            except ValueError:
                print(f"Input must be {type_.__name__}")
                continue

        # Check if the input is less than the minimum value
        if min_ is not None and user_input < min_:
            print(f"Input must be at least {min_}.")

        # Check if the input is more than the maximum value
        elif max_ is not None and user_input > max_:
            print(f"Input must be no more than {max_}.")

        # Check if the input is within the acceptable range of values
        elif range_ is not None and user_input not in range_:
            if isinstance(range_, range):
                template = "Input must be between {0.start} and {0.stop}."

            else:
                template = "Input must be {0}."

            print(template.format(range_))

        # If all conditions are satisfied, return the validated input
        else:
            return user_input


def run_aco(aco, graph):
    """
    This function runs the ACO algorithm and returns the best solution, its cost, and all solutions generated.

    Parameters:
    aco (ACO): The ACO algorithm object.
    graph (Graph): The graph representing our problem.

    Returns:
    tuple: A tuple containing the best solution, its cost, and all solutions generated.
    """
    # Initialize with high cost and empty solution
    best_cost = float('inf')
    best_solution = []
    all_solutions = []

    # Counter for stagnation in solutions
    stagnation_counter = 0
    best_costs = []

    # Prepare CSV file for saving cost progression
    with open('best_costs.csv', 'w', newline='') as csvfile:
        fieldnames = ['Generation', 'Best_Cost']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        # Main ACO loop
        for gen in range(aco.generations):
            solutions = aco.generate_solutions(graph)
            all_solutions.extend(solutions)

            # Keep track of the best cost in the previous generation
            previous_best_cost = best_cost

            # Find best cost in this generation
            for solution in solutions:
                cost = graph.cost(solution)
                if cost < best_cost:
                    best_cost = cost
                    best_solution = solution

            best_costs.append(best_cost)
            writer.writerow({'Generation': gen, 'Best_Cost': best_cost})

            # Check for stagnation
            if best_cost < previous_best_cost:
                stagnation_counter = 0
            else:
                stagnation_counter += 1

            # Stagnation condition for early stopping
            if stagnation_counter >= aco.max_stagnation:
                print(f'\nStopping early at generation {gen + 1} due to stagnation.')
                break

            # Update the pheromone trails
            aco.pheromone_update(graph, solutions)

    return best_solution, best_cost, all_solutions, best_costs


def run_elitist_aco(self, graph):
    """
    This function runs the Elitist ACO algorithm and returns the best solution, its cost, and all solutions generated.

    Parameters:
    graph (Graph): The graph representing our problem.

    Returns:
    tuple: A tuple containing the best solution, its cost, and all solutions generated.
    """
    # Initialize with high cost and empty solution
    best_cost = float('inf')
    best_solution = []
    all_solutions = []

    # Counter for stagnation in solutions
    stagnation_counter = 0
    best_costs = []

    # Prepare CSV file for saving cost progression
    with open('best_costs.csv', 'w', newline='') as csvfile:
        fieldnames = ['Generation', 'Best_Cost']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        # Main Elitist ACO loop
        for gen in range(self.generations):
            solutions = self.generate_solutions(graph)
            all_solutions.extend(solutions)

            # Keep track of the best cost in the previous generation
            previous_best_cost = best_cost

            # Find best cost in this generation
            for solution in solutions:
                cost = graph.cost(solution)
                if cost < best_cost:
                    best_cost = cost
                    best_solution = solution

            best_costs.append(best_cost)
            writer.writerow({'Generation': gen, 'Best_Cost': best_cost})

            # Check for stagnation
            if best_cost < previous_best_cost:
                stagnation_counter = 0
            else:
                stagnation_counter += 1

            # Stagnation condition for early stopping
            if stagnation_counter >= self.max_stagnation:
                print(f'\nStopping early at generation {gen + 1} due to stagnation.')
                break

            # Update the pheromone trails with additional pheromones for the best solution
            self.elitist_pheromone_update(graph, solutions, best_solution)

    return best_solution, best_cost, all_solutions, best_costs


def plot_path(self, best_path, cities, all_paths, line_width=1, point_radius=math.sqrt(2.0), annotation_size=8, dpi=120,
              save=True, filename='best_route.png'):
    """
    This function plots all the paths and the best path.

    Parameters:
    best_path (list): The best path.
    cities (list): The cities.
    all_paths (list): All paths.
    line_width (int, optional): The line width. Default is 1.
    point_radius (float, optional): The point radius. Default is sqrt(2.0).
    annotation_size (int, optional): The annotation size. Default is 8.
    dpi (int, optional): The dpi. Default is 120.
    save (bool, optional): Whether to save the plot or not. Default is False.
    filename (str, optional): The filename to save the plot as. Default is None.
    """
    # First, set up the figure and labels
    plt.figure(figsize=(8, 8))
    plt.title("Best Route (ACO)")
    plt.xlabel("x")
    plt.ylabel("y")

    # Plot all paths
    for path in all_paths:
        x = [cities[i].x for i in path]
        x.append(x[0])
        y = [cities[i].y for i in path]
        y.append(y[0])
        plt.plot(x, y, linewidth=line_width, color='0.85')

    # Highlight the best path in red
    x = [cities[i].x for i in best_path]
    x.append(x[0])
    y = [cities[i].y for i in best_path]
    y.append(y[0])
    plt.plot(x, y, linewidth=line_width, color='red')

    # Plot the cities as points
    plt.scatter([city.x for city in cities], [city.y for city in cities], s=math.pi * (point_radius ** 2.0))

    # Annotate the cities with their respective index numbers
    for i, city in enumerate(cities):
        plt.annotate(str(i), (city.x, city.y), size=annotation_size)

    # If save is True, save the figure to a file
    if save:
        if filename is None:
            filename = 'routes.png'
        plt.savefig(filename, dpi=dpi)

    # Display the plot
    plt.show()


def plot_costs(costs, filename='best_costs.png'):
    """
    This function generates and displays a plot of the cost over generations.

    Parameters:
    costs (list): The costs for each generation.
    filename (str, optional): The name of the file where the plot will be saved. Defaults to 'best_costs.png'.
    """
    # Set up the figure
    plt.figure()

    # Plot costs
    plt.plot(costs, color='red')

    # Set labels and title
    plt.ylabel('Distance')
    plt.xlabel('Generation')
    plt.title('Distance Over Generations')

    # Save the figure
    plt.savefig(filename)

    # Display the plot
    plt.show()


main()
