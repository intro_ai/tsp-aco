import random
from .ant import Ant


class ACO:
    def __init__(self, ant_count: int, generations: int, alpha: float, beta: float, rho: float, q: int, e: float, max_stagnation: int):
        """
        Initialize the ACO algorithm.

        Parameters:
        ant_count (int): The number of ants in the colony.
        generations (int): The number of generations to run the algorithm for.
        alpha (float): The pheromone importance factor.
        beta (float): The distance importance factor.
        rho (float): The pheromone evaporation rate.
        q (int): The total amount of pheromone that each ant lies on the trail per tour.
        e (float): The amount of extra pheromone for the best solution.
        max_stagnation (int): The stopping condition.
        """
        self.ant_count = ant_count
        self.generations = generations
        self.alpha = alpha
        self.beta = beta
        self.rho = rho
        self.q = q
        self.e = e
        self.max_stagnation = max_stagnation

    def generate_solutions(self, graph):
        """
        Generate solutions (routes) for all ants in the colony.

        Parameters:
        graph (Graph): The graph representing our problem.

        Returns:
        list: A list of solutions. Each solution is a list of city indices representing a route.
        """
        solutions = []

        for _ in range(self.ant_count):
            # Initialize an ant at a random city
            ant = Ant(random.randint(0, graph.num_cities - 1), graph.num_cities)

            # Let the ant create a route based on the pheromone levels and distances
            ant.create_route(graph, self.alpha, self.beta)

            # Add the ant's route to the list of solutions
            solutions.append(ant.route)

        return solutions

    def pheromone_update(self, graph, solutions):
        """
        Update the pheromone levels on the graph based on the solutions.

        Parameters:
        graph (Graph): The graph representing our problem.
        solutions (list): A list of solutions.
        """
        # Evaporate some of the pheromone on all edges of the graph
        graph.pheromone *= self.rho

        # Add pheromone to the edges that are part of the solutions
        # The amount of pheromone added depends on the quality of the solution (shorter is better)
        for solution in solutions:
            for a, b in zip(solution[:-1], solution[1:]):
                graph.add_pheromone(a, b, self.q / graph.cost(solution))

    def elitist_pheromone_update(self, graph, solutions, best_solution):
        """
        Update the pheromone levels on the graph based on the solutions, with additional weight given to the best solution.

        Parameters:
        graph (Graph): The graph representing our problem.
        solutions (list): A list of solutions.
        best_solution (list): The best solution found so far.
        """
        # Evaporate some of the pheromone on all edges of the graph
        graph.pheromone *= self.rho

        # Add pheromone to the edges that are part of the solutions
        for solution in solutions:
            for a, b in zip(solution[:-1], solution[1:]):
                graph.add_pheromone(a, b, self.q / graph.cost(solution))

        # Elitist update: Add additional pheromone for the best solution
        if self.e is not None:  # Check that e was provided
            for a, b in zip(best_solution[:-1], best_solution[1:]):
                graph.add_pheromone(a, b, self.e / graph.cost(best_solution))

