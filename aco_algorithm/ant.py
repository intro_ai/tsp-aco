import numpy as np


class Ant:
    def __init__(self, start_city, num_cities):
        """
        Constructs an Ant object.

        Parameters:
        start_city (int): The index of the start city.
        num_cities (int): The total number of cities.
        """
        self.route = [start_city]
        self.total_distance = 0.0
        self.num_cities = num_cities

    def create_route(self, graph, alpha, beta):
        """
        Create a route for the ant based on pheromone and distance information.

        Parameters:
        graph (Graph): The graph representing the problem.
        alpha (float): The influence of pheromone on the ant's decision.
        beta (float): The influence of distance on the ant's decision.
        """
        # Set of all cities
        all_cities = set(range(self.num_cities))

        while len(self.route) < self.num_cities:
            # Current city
            i = self.route[-1]

            # List of unvisited cities
            possible_cities = list(all_cities - set(self.route))

            probabilities = []
            for j in possible_cities:
                # Calculate the pheromone and distance influence on the ant's decision
                pheromone_influence = graph.get_pheromone(i, j) ** alpha
                distance_influence = (1.0 / graph.get_distance(i, j)) ** beta
                probability = pheromone_influence * distance_influence
                probabilities.append(probability)

            # Normalize the probabilities
            probabilities = [prob / sum(probabilities) for prob in probabilities]

            # Select the next city based on probabilities
            next_city = np.random.choice(possible_cities, 1, p=probabilities)[
                0]

            self.route.append(next_city)
            self.total_distance += graph.get_distance(i, next_city)

        self.route.append(self.route[0])
        self.total_distance += graph.get_distance(self.route[-1], self.route[0])

