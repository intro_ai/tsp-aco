import numpy as np


class City:
    def __init__(self, x, y):
        """
        Constructs a City object with given x and y coordinates.

        Parameters:
        x (float): The x-coordinate of the city.
        y (float): The y-coordinate of the city.
        """
        self.x = x
        self.y = y

    def distance(self, city):
        """
        Computes the Euclidean distance between this city and another city.

        Parameters:
        city (City): The other city.

        Returns:
        float: The distance between the two cities.
        """
        return np.sqrt((self.x - city.x) ** 2 + (self.y - city.y) ** 2)


def generate_cities(num_cities=25, plane_size=200, seed=42):
    """
    Generates a list of cities at random coordinates.

    Parameters:
    num_cities (int, optional): The number of cities to generate. Defaults to 25.
    plane_size (float, optional): The size of the plane in which to generate cities. Defaults to 200.
    seed (int, optional): The seed for the random number generator. Defaults to 42.

    Returns:
    list: A list of City objects.
    """
    cities = [City(np.random.uniform(0, plane_size), np.random.uniform(0, plane_size)) for _ in range(num_cities)]

    return cities


def create_distances(cities):
    """
    Creates a distance matrix for a given list of cities.

    Parameters:
    cities (list): A list of City objects.

    Returns:
    np.array: A 2D numpy array representing the distances between each pair of cities.
    """
    num_cities = len(cities)
    distances = np.zeros((num_cities, num_cities))

    for i in range(num_cities):
        for j in range(i + 1, num_cities):
            distances[i][j] = cities[i].distance(cities[j])
            distances[j][i] = distances[i][j]

    return distances
