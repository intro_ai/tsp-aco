class Graph:
    def __init__(self, num_cities, distances, initial_pheromone):
        """
        Constructs a Graph object.

        Parameters:
        num_cities (int): The total number of cities.
        distances (np.array): A 2D numpy array representing the distances between each pair of cities.
        initial_pheromone (np.array): A 2D numpy array representing the initial pheromone levels for each pair of cities.
        """
        self.num_cities = num_cities
        self.distances = distances
        self.pheromone = initial_pheromone

    # This calculates the total distance of a given route.
    def cost(self, route):
        """
         Computes the total cost of a given route.

         Parameters:
         route (list): A list of city indices representing the route.

         Returns:
         float: The total cost of the route.
         """
        total = 0

        for a, b in zip(route[:-1], route[1:]):
            total += self.distances[a][b]

        return total

    def get_distance(self, i, j):
        """
        Gets the distance between two cities.

        Parameters:
        i, j (int): The indices of the two cities.

        Returns:
        float: The distance between the two cities.
        """
        return self.distances[i][j]

    def get_pheromone(self, i, j):
        """
        Gets the amount of pheromone on the path between two cities.

        Parameters:
        i, j (int): The indices of the two cities.

        Returns:
        float: The amount of pheromone on the path.
        """
        return self.pheromone[i][j]

    def add_pheromone(self, i, j, amount):
        """
        Adds a certain amount of pheromone to the path between two cities.

        Parameters:
        i, j (int): The indices of the two cities.
        amount (float): The amount of pheromone to add.
        """
        self.pheromone[i][j] += amount
